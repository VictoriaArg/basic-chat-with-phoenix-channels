defmodule ChatWeb.PageController do
  use ChatWeb, :controller

  alias ChatWeb.ChatPage

  import Phoenix.LiveView.Controller

  def index(conn, _params) do
    live_render(conn, ChatPage)
  end
end
