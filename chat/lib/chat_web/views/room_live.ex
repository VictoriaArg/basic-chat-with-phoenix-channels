defmodule ChatWeb.RoomLive do
  use ChatWeb, :live_view

  require Logger

  @impl true
  def mount(%{"id" => room_id}, _session, socket) do
    topic = "room:" <> room_id
    user = MnemonicSlugs.generate_slug(2)

    if connected?(socket) do
      ChatWeb.Endpoint.subscribe(topic)
      ChatWeb.Presence.track(self(), topic, user, %{})
    end

    updated_socket =
      socket
      |> assign(topic: topic)
      |> assign(room_id: room_id)
      |> assign(current_message: "")
      |> assign(user: user)
      |> assign(users: [user])
      |> assign(messages: [])

    {:ok, updated_socket, temporary_assigns: [messages: [], users: []]}
  end

  @impl true
  def handle_event("submit_message", %{"chat" => %{"message" => message}}, socket) do
    new_message = %{uuid: UUID.uuid4(), content: message, user: socket.assigns.user}
    ChatWeb.Endpoint.broadcast(socket.assigns.topic, "new_message", new_message)
    {:noreply, assign(socket, :current_message, "")}
  end

  def handle_event("form_update", %{"chat" => %{"message" => message}}, socket) do
    {:noreply, assign(socket, :current_message, message)}
  end

  @impl true
  def handle_info(%{event: "new_message", payload: message}, socket) do
    {:noreply, assign(socket, messages: [message])}
  end

  @impl true
  def handle_info(%{event: "presence_diff", payload: %{joins: joins, leaves: leaves}}, socket) do
    join_messages =
      joins
      |> Map.keys()
      |> Enum.map(fn user ->
        %{uuid: UUID.uuid4(), content: "#{user} joined", user: "system"}
      end)

    leave_messages =
      leaves
      |> Map.keys()
      |> Enum.map(fn user ->
        %{uuid: UUID.uuid4(), content: "#{user} left", user: "system"}
      end)

    user_list =
      socket.assigns.topic
      |> ChatWeb.Presence.list()
      |> Map.keys()

    {:noreply, assign(socket, messages: join_messages ++ leave_messages, users: user_list)}
  end
end
